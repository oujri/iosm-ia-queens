/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iaqueens;

import java.util.Scanner;

/**
 *
 * @author oujri
 */
public class IAQueens {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(true){
            System.err.print("Enter your n : ");
            int n = sc.nextInt();
            createBoard(n);
        }
    }

    private static void createBoard(int n) {
        // If n <= 3 then solution is not possible as
        if (n < 4) {
            System.out.println("No Solution available\n\n");
        } else {
            int[][] board = new int[n][n];
            createQueens(board, 0);
            printBoard(board);
        }
    }

    private static boolean createQueens(int board[][], int row) {
        if (row >= board.length) {
            return true;
        }

        boolean completed = false;
        for (int j = 0; j < board.length; j++) {

            if (allowed(board, row, j)) {
                board[row][j] = 1;
                completed = createQueens(board, row + 1);
            }
            if (completed) {
                break;
            } else {
                board[row][j] = 0;
            }
        }
        return completed;
    }

    private static boolean allowed(int board[][], int row, int col) {

        // Check Left Upper Diagonal
        for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; i--, j--) {
            if (board[i][j] == 1) {
                return false;
            }
        }

        // Check Right Upper Diagonal
        for (int i = row - 1, j = col + 1; i >= 0 && j < board.length; i--, j++) {
            if (board[i][j] == 1) {
                return false;
            }
        }

        // Check in same Column
        for (int i = row - 1; i >= 0; i--) {
            if (board[i][col] == 1) {
                return false;
            }
        }

        return true;
    }

    private static void printBoard(int[][] board) {
        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board.length; col++) {
                if (board[row][col] == 1) {
                    System.out.print(" Ǒ ");
                } else {
                    System.out.print(" - ");
                }
            }
            System.out.println();
        }
        System.out.println("\n\n");
    }
}
